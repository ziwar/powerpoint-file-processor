var fs      = require('fs');
var vm      = require('vm');
var path   = require('path');

var xmldom = require('xmldom');

include(__dirname + "/linq.js");
include(__dirname + "/ltxml.js");
include(__dirname + "/ltxml-extensions.js");
var openXml = require(__dirname + '/openxml');

var DOMParser = require('xmldom').DOMParser;
Ltxml.DOMParser = DOMParser;

var XAttribute = Ltxml.XAttribute;
var XCData = Ltxml.XCData;
var XComment = Ltxml.XComment;
var XContainer = Ltxml.XContainer;
var XDeclaration = Ltxml.XDeclaration;
var XDocument = Ltxml.XDocument;
var XElement = Ltxml.XElement;
var XName = Ltxml.XName;
var XNamespace = Ltxml.XNamespace;
var XNode = Ltxml.XNode;
var XObject = Ltxml.XObject;
var XProcessingInstruction = Ltxml.XProcessingInstruction;
var XText = Ltxml.XText;
var XEntity = Ltxml.XEntity;
var cast = Ltxml.cast;
var castInt = Ltxml.castInt;

var W = openXml.W;
var NN = openXml.NoNamespace;
var wNs = openXml.wNs;


function getSlideIdList(presentation)
{
    for (var i=0; i < presentation.nodesArray.length; i++) {

        if(presentation.nodesArray[i].name.localName == 'sldIdLst') {
            return presentation.nodesArray[i].nodesArray;
        }
    }

    return [];
}
function getMaxIdFromChild(slideIds)
{
    var max = 1;
    for (var i = 0; i < slideIds.length; i++) {
        var id = parseInt(slideIds[i].getFirstAttribute().simpleValue);
        if(id > max)
        {
            max = id;
        }

    }
    return max;
    
}

function fixSlideLayoutIds(presentationPart)
{

    var slideMasterParts = presentationPart.getPartsByContentType("application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml");

    for (var i = 0; i < slideMasterParts.length; i++) {
        var layouts = slideMasterParts[i].getXDocument().root.nodesArray[2].nodesArray;
        for (var j = 0; j < layouts.length; j++) {
            uniqueId++;
            layouts[j].attributesArray[0].simpleValue == uniqueId;
        };
    };
}

function mergeFile(combinedFile,slide)
{


    var slidePresentationPart = slide.presentationPart();
    var slidePresentation = slide.presentationPart().getXDocument().getRoot();
    var combinedPresentation = combinedFile.presentationPart().getXDocument().getRoot();

    var slides = [];
    var combinedPresentationSlides = [];
    slidePresentation.elements().forEach(function(s){
        if(s.name.toString() == "{http://schemas.openxmlformats.org/presentationml/2006/main}sldIdLst")
            slides = s.nodesArray;
    })

    combinedPresentation.elements().forEach(function(s){
        if(s.name.toString() == "{http://schemas.openxmlformats.org/presentationml/2006/main}sldIdLst")
            combinedPresentationSlides = s.nodesArray;
    })
    combinedPresentationSlides.push(slides[0]);
  

    var relId = slides[0].attributesArray[1].simpleValue;

    var slidePart = slidePresentationPart.getPartById(relId);

    combinedFile.addPart(slidePart.uri,"application/vnd.openxmlformats-officedocument.presentationml.slide+xml","xml",slidePart.data);

}

exports.merge = function(mainPresentationFile,callback)
{
    
        var extname = path.extname(mainPresentationFile);
        
        var combinedDoc = new openXml.OpenXmlPackage(mainPresentationFile);  
        var presentationPart = combinedDoc.presentationPart();
       
        var slideCount = presentationPart.slideParts().length;

        deleteAll(combinedDoc);
        
        for (var i = 0; i < slideCount; i++) {
            
            var fileToMerge = mainPresentationFile.replace(extname, '_' + pad(i,3) + extname);
            console.log(i);

            var slideDoc = new openXml.OpenXmlPackage(fileToMerge);  

            mergeFile(combinedDoc,slideDoc);

            slideDoc = null; 
        };

        combinedDoc.saveToZip(mainPresentationFile, function(err,res){

            callback(err);

        });
        
  
}

exports.split = function(filename,callback){

    splitSlide(filename,0,callback);
}

function splitSlide(openedFileName,index,callback)
{
    
    var newDoc = new openXml.OpenXmlPackage(openedFileName);

    var slides = newDoc.presentationPart().slideParts();

    var slideCount = slides.length;

    if(index >= slides.length) return callback(null,slideCount);


    var uri = '/ppt/slides/slide' +(index+1)+ '.xml';

    var part = newDoc.getPartByUri(uri);
    
    if(!part) return callback("Error: part not found: " + uri); 

    deleteAllExecpt(newDoc, part);

    var extname = path.extname(openedFileName);
    var name = openedFileName.replace(extname, '_' + pad(index,3) + extname);
    newDoc.saveToZip(name, function(err,res){

        if(!err){
            newDoc = null;
            splitSlide(openedFileName,index+1,callback);
        }
        else
            callback(err);

    });

   
}     
function include(path) {
    var code = fs.readFileSync(path, 'utf-8');
    vm.runInThisContext(code, path);
};

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

var deleteAll = function(doc){
    var presentation = doc.presentationPart().getXDocument().getRoot();
    var slides = [];

    presentation.elements().forEach(function(s){
        if(s.name.toString() == "{http://schemas.openxmlformats.org/presentationml/2006/main}sldIdLst")
            slides = s.nodesArray;
    })

    for (var i = 0; i < slides.length; i++) {

            var p = doc.presentationPart().getPartById(slides[i].attributesArray[1].simpleValue);
            doc.deletePart(p);
            
       
    };

    slides.length = 0;

}
var deleteAllExecpt = function(doc, part){
    
    var relations = doc.presentationPart().getRelationshipsByContentType("application/vnd.openxmlformats-officedocument.presentationml.slide+xml")
    var uri = part.uri;
    
    console.log(uri)
    
    var rid = ""; 
    for (var i = 0; i < relations.length; i++) {
        if(relations[i].targetFullName == uri){
            rid = relations[i].relationshipId;
            break;
        }
    };

    var presentation = doc.presentationPart().getXDocument().getRoot();
    var slides = [];

    presentation.elements().forEach(function(s){
        if(s.name.toString() == "{http://schemas.openxmlformats.org/presentationml/2006/main}sldIdLst")
            slides = s.nodesArray;
    })

    for (var i = 0; i < slides.length; i++) {

        if(slides[i].attributesArray[1].simpleValue != rid){
            var p = doc.presentationPart().getPartById(slides[i].attributesArray[1].simpleValue);
            doc.deletePart(p);
            
        }
        else
            slide = slides[i];
    };

    slides.length = 0;
    slides.push(slide);
    doc.presentationPart().slideParts().length = 0;
    doc.presentationPart().slideParts().push(part);


}

 //exports.split ('/Users/mblm/Documents/roadshow/222.pptx',function(){

//     exports.merge ('/Users/mblm/Documents/243/243.pptx',function(){});
 //});

   

